=================
Website Templates
=================

:author: Curtis Sand <curtissand@gmail.com>

:lastedit: 20181202-1330

Website is a basic website project. Website Templates, is an auxilliary
repository of, well, website templates. The idea is to provide a number of
different layouts and behaviours that can be used to start a project.

Extras
======

Stand-in Images
---------------

Making stand in images to use while building projects. This trick uses a bash
loop and the ImageMagick toolset to build images that you can use in your
projects until you have actual production content to take its place. ::

    for i in 0 1 2 3 4 5 6 7 8 9 A B C D E F G H I J \
            K L M N O P Q R S T U V W X Y Z ; do \
        echo $i; \
        convert -size 50x50 \
                xc:lightblue \
                -pointsize 40 \
                -fill blue \
                -gravity center \
                -draw "text 0,0 '$i'" $i.png; \
    done
